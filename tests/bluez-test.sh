#!/bin/sh

. common/update-test-path

set -eux

. common/common-bluez.sh

enumerate_devices

if [ "${DEV_COUNT}" -lt "2" ]; then
	echo "Need 2 bluetooth devices"
	exit 1
fi

strip_quote() {
	echo $* | sed -e "s/.*'\(.*\)'.*/\1/"
}

strip_variant() {
	echo $* | sed -e "s/.*<\(.*\)>.*/\1/"
}

strip_type() {
	echo $* | sed -e "s/.*<.* \(.*\)>.*/\1/"
}

TestManager()
{
	OBJECT_MANAGER="gdbus call --system --dest org.bluez --object-path / --method org.freedesktop.DBus.ObjectManager"
	OBJECTS=$(${OBJECT_MANAGER}.GetManagedObjects)
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'/org/bluez/hci0':")
			echo "Adapter object found"
			;;
		esac
	done
}

test_adapter()
{
	# Sanity check
	${PROPS}.Get "org.bluez.Adapter1" "DumbPropertyDoesNotExist" && exit 1 || true

	${PROPS}.Get "org.bluez.Adapter1" "Address"
	${PROPS}.Get "org.bluez.Adapter1" "Name"
	${PROPS}.Get "org.bluez.Adapter1" "Alias"
	${PROPS}.Get "org.bluez.Adapter1" "Class"
	${PROPS}.Get "org.bluez.Adapter1" "Powered"
	${PROPS}.Get "org.bluez.Adapter1" "Discoverable"
	${PROPS}.Get "org.bluez.Adapter1" "Pairable"
	${PROPS}.Get "org.bluez.Adapter1" "PairableTimeout"
	${PROPS}.Get "org.bluez.Adapter1" "DiscoverableTimeout"
	${PROPS}.Get "org.bluez.Adapter1" "Discovering"
	${PROPS}.Get "org.bluez.Adapter1" "UUIDs"
}

test_adapter_alias()
{
	${PROPS}.Get "org.bluez.Adapter1" "Alias"
	${PROPS}.Set "org.bluez.Adapter1" "Alias" "<'unittest-0'>"
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Alias")
	PROP=$(strip_quote ${PROP})
	# Workaround asynchronims in Alias property, try again after 1 second if it's  not pass
	if [ "$PROP" != "unittest-0" ] ; then
		sleep 1
		echo "Retrying alias retrieval"
		PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Alias")
		PROP=$(strip_quote ${PROP})
	fi
	test "$PROP" = "unittest-0"
}

test_adapter_class()
{
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Class")
	PROP=$(strip_type ${PROP})
	COD=$(expr ${PROP} % 65536)
	test "$COD" != "0"
}

test_adapter_discoverable()
{
	${PROPS}.Set "org.bluez.Adapter1" "Discoverable" "<false>"
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Discoverable")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "false"

	hciconfig ${NAME0} | grep -v ISCAN

	${PROPS}.Set "org.bluez.Adapter1" "Discoverable" "<true>"

	hciconfig ${NAME0} | grep ISCAN

	${PROPS}.Set "org.bluez.Adapter1" "Discoverable" "<false>"

	hciconfig ${NAME0} | grep -v ISCAN

	${PROPS}.Set "org.bluez.Adapter1" "DiscoverableTimeout" "<uint32 5>"
	${PROPS}.Set "org.bluez.Adapter1" "Discoverable" "<true>"
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Discoverable")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "true"

	hciconfig ${NAME0} | grep ISCAN

	sleep 8

	${PROPS}.Set "org.bluez.Adapter1" "DiscoverableTimeout" "<uint32 180>"
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Discoverable")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "false"

	hciconfig ${NAME0} | grep -v ISCAN

	PROP=$(strip_quote $PROP)
}

test_adapter_pairable()
{
	${PROPS}.Set "org.bluez.Adapter1" "Pairable" "<false>"

	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Pairable")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "false"

	${PROPS}.Set "org.bluez.Adapter1" "Pairable" "<true>"
	${PROPS}.Set "org.bluez.Adapter1" "Pairable" "<false>"
	${PROPS}.Set "org.bluez.Adapter1" "PairableTimeout" "<uint32 5>"
	${PROPS}.Set "org.bluez.Adapter1" "Pairable" "<true>"
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Pairable")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "true"

	sleep 8

	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Pairable")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "false"

	PROP=$(strip_quote $PROP)
}

test_adapter_powered()
{
	${PROPS}.Set "org.bluez.Adapter1" "Powered" "<false>"
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Powered")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "false"

	hciconfig ${NAME0} | grep DOWN

	${PROPS}.Set "org.bluez.Adapter1" "Powered" "<true>"
	PROP=$(${PROPS}.Get "org.bluez.Adapter1" "Powered")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "true"

	hciconfig ${NAME0} | grep UP
}

test_adapter_discover()
{
	# Pair two make one device discoverable and pair the other one
	pair_two ${NAME0} ${NAME1}

	# Do it in both directions (scan on the other adapter)
	pair_two ${NAME1} ${NAME0}
}

test_adapter_pairing()
{
	# Pair two make one device discoverable and pair the other one
	pair_two ${NAME0} ${NAME1}

	sudo hciconfig ${NAME0} sspmode 0

	# Pair two make one device discoverable and pair the other one
	pair_two ${NAME0} ${NAME1}

	sudo hciconfig ${NAME0} sspmode 1
}

TestAdapter()
{
	OBJECT_MANAGER="gdbus call --system --dest org.bluez --object-path / --method org.freedesktop.DBus.ObjectManager"
	OBJECTS=$(${OBJECT_MANAGER}.GetManagedObjects)
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'/org/bluez/hci0':")
			ADAPTER0=$(strip_quote ${WORD})
			NAME0=hci0
			echo "hci0 object found"
			;;
		"'/org/bluez/hci1':")
			ADAPTER1=$(strip_quote ${WORD})
			NAME1=hci1
			echo "hci1 object found"
			;;
		esac
	done

	PROPS="gdbus call --system --dest org.bluez --object-path ${ADAPTER0} --method org.freedesktop.DBus.Properties"

	test_adapter
	test_adapter_alias
	test_adapter_class
	test_adapter_discover
	test_adapter_discoverable
	test_adapter_pairable
	test_adapter_pairing
	test_adapter_powered
}

test_device()
{
	# Sanity check
	${PROPS}.Get "org.bluez.Device1" "DumbPropertyDoesNotExist" && exit 1 || true

	# Check for non-optional properties.
	${PROPS}.Get "org.bluez.Device1" "Adapter"
	${PROPS}.Get "org.bluez.Device1" "Address"
	${PROPS}.Get "org.bluez.Device1" "Alias"
	${PROPS}.Get "org.bluez.Device1" "Blocked"
	${PROPS}.Get "org.bluez.Device1" "Connected"
	${PROPS}.Get "org.bluez.Device1" "Paired"
	${PROPS}.Get "org.bluez.Device1" "Trusted"
}

test_device_trusted()
{
	${PROPS}.Set "org.bluez.Device1" "Trusted" "<true>"
	PROP=$(${PROPS}.Get "org.bluez.Device1" "Trusted")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "true"

	${PROPS}.Set "org.bluez.Device1" "Trusted" "<false>"
	PROP=$(${PROPS}.Get "org.bluez.Device1" "Trusted")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "false"
}

test_device_blocked()
{
	${PROPS}.Set "org.bluez.Device1" "Blocked" "<true>"
	PROP=$(${PROPS}.Get "org.bluez.Device1" "Blocked")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "true"

	${PROPS}.Set "org.bluez.Device1" "Blocked" "<false>"
	PROP=$(${PROPS}.Get "org.bluez.Device1" "Blocked")
	PROP=$(strip_variant ${PROP})
	test "$PROP" = "false"
}

TestDevice()
{
	ADAPTER="gdbus call --system --dest org.bluez --object-path / --method org.freedesktop.DBus.ObjectManager"
	OBJECTS=$(${ADAPTER}.GetManagedObjects)
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'/org/bluez/hci0/dev_"*"':")
			DEVICE0=$(strip_quote ${WORD})
			echo "device object found"
			;;
		esac
	done

	PROPS="gdbus call --system --dest org.bluez --object-path ${DEVICE0} --method org.freedesktop.DBus.Properties"

	test_device
	test_device_trusted
	test_device_blocked
}

TestManager
TestAdapter
TestDevice
