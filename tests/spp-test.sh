#!/bin/sh

. common/update-test-path
. common/common-bluez.sh

enumerate_devices

echo "DEV COUNT:${DEV_COUNT}"

if [ "${DEV_COUNT}" -lt "2" ]; then
	echo "Need 2 bluetooth devices"
	exit 1
fi

pair_two ${NAME0} ${NAME1}
if [ $? -ne 0 ] ; then
	echo "Pairing failed"
	exit 1
fi

echo -n "Test 1: No encryption ... "
rctest -i ${NAME0} -a ${NAME1} -B $TESTPATH/rc.in -O /tmp/rc.out -b 20000 -P 27 2> /dev/null
sleep 1
ERR=$(diff -q $TESTPATH/rc.in /tmp/rc.out 2>&1)
if [ $? -ne 0 ] ; then
    echo "FAILED"
    echo $ERR 1>&2
else
    echo "PASSED"
fi
rm -f /tmp/rc.out

echo -n "Test 2: Encryption ... "
rctest -i ${NAME0} -a ${NAME1} -B $TESTPATH/rc.in -O /tmp/rc.out -b 20000 -P 28 -E 2> /dev/null
sleep 1
ERR=$(diff -q $TESTPATH/rc.in /tmp/rc.out 2>&1)
if [ $? -ne 0 ] ; then
    echo "FAILED"
    echo $ERR 1>&2
else
    echo "PASSED"
fi
rm -f /tmp/rc.out

echo -n "Test 3: Secure ... "
rctest -i ${NAME0} -a ${NAME1} -B $TESTPATH/rc.in -O /tmp/rc.out -b 20000 -P 29 -S 2> /dev/null
sleep 1
ERR=$(diff -q $TESTPATH/rc.in /tmp/rc.out 2>&1)
if [ $? -ne 0 ] ; then
    echo "FAILED"
    echo $ERR 1>&2
else
    echo "PASSED"
fi
rm -f /tmp/rc.out
