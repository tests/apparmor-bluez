# Need to source this file.

enumerate_devices() {

	DEVSFILE=$(mktemp)
	hcitool dev | grep "hci" > ${DEVSFILE}

	COUNT=0

	TMPFILE=$(mktemp)

	echo "TEMP FILE: ${TMPFILE}"

	while read NAME ID; do
		if [ "${NAME}" != "" ]; then
			echo "NAME${COUNT}=\"${NAME}\"" >> ${TMPFILE}
			echo "ID${COUNT}=\"${ID}\"" >> ${TMPFILE}
			COUNT=$((COUNT + 1))
		fi
	done < ${DEVSFILE}

	echo "DEV_COUNT=${COUNT}" >> ${TMPFILE}

	. ${TMPFILE}
}
