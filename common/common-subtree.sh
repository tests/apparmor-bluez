#!/bin/sh

usage () {
	echo "usage: $0 [add|pull]"
	echo ""
	echo "'$0 add' adds the common subtree to a tests repository"
	echo "'$0 pull' is used to update the common subtree in tests"
}

branch="$(git symbolic-ref --short HEAD)"
test -z "$branch" && branch="master"

case $1 in
pull)
git subtree pull -P common git@gitlab.apertis.org:tests/common.git "$branch"
;;
add)
git subtree add -P common git@gitlab.apertis.org:tests/common.git "$branch"
;;
*) usage;;
esac
