Common files shared accross apertis tests.

Those files are shared through a git subtree which is included by all test projects.

Use `common-subtree.sh add` to add the subtree in a tests repository.
For example:
```
# Retrieve the common-subtree.sh script, we use `git`, but could also use `wget`
$ git clone git@gitlab.apertis.org:tests/common

# Clone the test repository if needed
$ git clone git@gitlab.apertis.org:tests/iptables-basic
# Enter the test directory
$ cd iptables-basic

# In the test directory, add the common git subtree
$ ../common/common-subtree.sh add

# A new commit has been created, push the test with the subtree initialized 
$ git push origin
```

If a script is supposed to be in the common/ directory, it should be commited in the
[common repository](https://gitlab.apertis.org/tests/common), and then it can be pulled
into the git subtree using the command:
```
$ ../common/common-subtree.sh pull

```
